SQL-wrapper for SQLite3
====
Find it hard to use SQLite3 library in C? This wrapper makes you life easier by adopting syntaxes from many other known languages.

Try it out after cloning: 

```
make
$ ./sql-test
```

Remember to install SQLite3 first, and notice the linker option in the makefile - to link everything together. On Debian based systems:

```
$ sudo apt-get install sqlite3
```

Enjoy.
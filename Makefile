# MAKEFILE

CC=gcc
CCOPTS=-Wall -g
CCL=-lpthread -lm 

SQL_SRC=sql.c
SQL_HEADERS=sql.h

COMMON_SRC=common.c
COMMON_HEADER=common.h

all: sql-test

sql-test: $(SQL_SRC) $(COMMON_SRC) $(SQL_HEADERS) $(COMMON_HEADER) Makefile
	$(CC) $(CCOPTS) $(SQL_SRC) $(COMMON_SRC) -o $@ -D_DEBUG_SQL=1 -include $(SQL_HEADERS) $(COMMON_HEADER) $(CCL) -lsqlite3

clean:
	rm -f *~ *.o *.exe *.stackdump sql-* json-* *.dot* *.txt
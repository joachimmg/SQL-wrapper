#ifndef _SQL_H
#define _SQL_H

#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>


#define SQL_NOERR_LIMIT 	1
#define SQL_OK 				SQL_NOERR_LIMIT + SQLITE_OK
#define SQL_ROW				SQL_NOERR_LIMIT + SQLITE_ROW
#define SQL_DONE			SQL_NOERR_LIMIT + SQLITE_DONE

#define SQL_ERR_LIMIT		-6667
#define SQL_ERROR 			SQL_ERR_LIMIT + SQLITE_ERROR
#define SQL_INTERNAL 		SQL_ERR_LIMIT + SQLITE_INTERNAL
#define SQL_PERM 			SQL_ERR_LIMIT + SQLITE_PERM
#define SQL_ABORT 			SQL_ERR_LIMIT + SQLITE_ABORT
#define SQL_BUSY 			SQL_ERR_LIMIT + SQLITE_BUSY
#define SQL_LOCKED 			SQL_ERR_LIMIT + SQLITE_LOCKED
#define SQL_NOMEM 			SQL_ERR_LIMIT + SQLITE_NOMEM
#define SQL_READONLY 		SQL_ERR_LIMIT + SQLITE_READONLY
#define SQL_INTERRUPT 		SQL_ERR_LIMIT + SQLITE_INTERRUPT
#define SQL_IOERR			SQL_ERR_LIMIT + SQLITE_IOERR
#define SQL_CORRUPT			SQL_ERR_LIMIT + SQLITE_CORRUPT
#define SQL_NOTFOUND 		SQL_ERR_LIMIT + SQLITE_NOTFOUND
#define SQL_FULL 			SQL_ERR_LIMIT + SQLITE_FULL
#define SQL_CANTOPEN 		SQL_ERR_LIMIT + SQLITE_CANTOPEN
#define SQL_PROTOCOL 		SQL_ERR_LIMIT + SQLITE_PROTOCOL
#define SQL_EMPTY 			SQL_ERR_LIMIT + SQLITE_EMPTY
#define SQL_SCHEMA 			SQL_ERR_LIMIT + SQLITE_SCHEMA
#define SQL_TOOBIG			SQL_ERR_LIMIT + SQLITE_TOOBIG
#define SQL_CONSTRAINT		SQL_ERR_LIMIT + SQLITE_CONSTRAINT
#define SQL_MISMATCH 		SQL_ERR_LIMIT + SQLITE_MISMATCH
#define SQL_MISUSE			SQL_ERR_LIMIT + SQLITE_MISUSE
#define SQL_NOLFS			SQL_ERR_LIMIT + SQLITE_NOLFS
#define SQL_AUTH			SQL_ERR_LIMIT + SQLITE_AUTH
#define SQL_FORMAT			SQL_ERR_LIMIT + SQLITE_FORMAT
#define SQL_RANGE			SQL_ERR_LIMIT + SQLITE_RANGE
#define SQL_NOTADB			SQL_ERR_LIMIT + SQLITE_NOTADB
/* RESULTCODES > 101 is IGNORED */

#define SQL_OUTOFMEM 		SQL_ERR_LIMIT - 1
#define SQL_NULL_ABORTION	SQL_ERR_LIMIT - 2

/**
 * Structure: sql_db
 * Typedef: sql_db_t
 * Author: Joachim M. Giæver
 *
 * Description:
 * 	Stores information about an SQL-handler and it relations.
 **/
typedef struct sql_db sql_db_t;

/**
 * Structure: sql_stmt
 * Typedef: sql_stmt_t
 * Author: Joachim M. Giæver
 *
 * Description:
 * 	Stores information about an SQL-statement and its relations.
 *
 **/
typedef struct sql_stmt sql_stmt_t;

/**
 * Function: sql_create
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - char *db_file:		The DB-file to open
 * - int flags:			The flags, doesnt support MUTEX and CACHE flags
 *
 * Description:
 * Creates an SQL-object; init the SQL
 *
 * Returns: sql_db_t *, db object
 **/
sql_db_t *sql_create( char *db_file, int flags );

/**
 * Function: sql_ok
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_db_t *sql: 	The SQL-handler to check.
 *
 * Description:
 * Checks if last activity agains the DB (e.g a query)
 * was successfull.
 *
 * Returns: int, 1 on success, 0 on failure
 **/
int sql_ok( sql_db_t *sql );

/**
 * Function: sql_done
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_db_t *sql: 	The SQL-handler to check.
 *
 * Description:
 * Determines if the SQL-task is done.
 *
 * Returns: int, 1 on success, 0 on failure
 **/
int sql_done( sql_db_t *sql );

/**
 * Function: sql_errmsg
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_db_t *sql: 	The SQL-handler to look for errors in.
 *
 * Description:
 * Returns the corresponding SQL-error message.
 * If sql_ok() returns an error, there is an
 * error present.
 *
 * Returns: char *, the error message
 **/
const char *sql_errmsg( sql_db_t *sql );

/**
 * Function: sql_close
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_db_t *sql: 	The SQL-handler to close
 *
 * Description:
 * Close an SQL-handler. This will also free related
 * statements for the handler, which as unessacary.
 * Statements should be finalized by designer, but are
 * implemented as a "gb-collection".
 *
 * Returns: void
 **/
void sql_close( sql_db_t *sql );

/**
 * Function: sql_query
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_db_t *sql: 	The SQL-handler to query agains
 * - const char *query: The SQL-statement/-query
 *
 * Description:
 * Creates an SQL-statement and queries the DB.
 *
 * Returns: sql_stmt_t *, the SQL-statement object.
 **/
sql_stmt_t *sql_query( sql_db_t *sql, const char *query );

/**
 * Function: sql_column_count
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_stmt_t *stmt: 	SQL-statement to check
 *
 * Description:
 * Returns the amount of colums returned from a query
 *
 * Returns: int, #colums returned
 **/
int sql_column_count( sql_stmt_t *stmt );

/**
 * Function: sql_column_name
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_stmt_t *stmt: 	SQL-statment to read from
 *
 * Description:
 * Returns the columname specified by the index.
 *
 * Returns: char *, colum name
 **/
const char *sql_column_name( sql_stmt_t *stmt, int idx );

/**
 * Function: sql_column_text
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_stmt_t *stmt:	SQL-statement to read from
 *
 * Description:
 * Return the text stored in the column specified by the index.
 *
 * Returns: const unsigned char *, column text
 **/
const unsigned char *sql_column_text( sql_stmt_t *stmt, int idx );

/**
 * Function: sql_stmt_step
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_stmt_t *stmt: 	SQL-statement to read from
 *
 * Description:
 * Go to next entry in the retured query.
 *
 * Returns: int, 1 on success, 0 on failure.
 **/
int sql_stmt_step( sql_stmt_t *stmt );

/**
 * Function: sql_stmt_sql
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_stmt_t *stmt: 	SQL-statement to load handler from
 *
 * Description:
 * Returns the related SQL-handler for the SQL-statement.
 *
 * Returns: sql_db_t *, SQL-object
 **/
sql_db_t *sql_stmt_sql( sql_stmt_t *stmt );

/**
 * Function: sql_stmt_tail
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_stmt_t *stmt: 	The parent statement
 *
 * Description:
 * Queries the SQL with the next tail-query.
 *
 * Note: This finalizes the parent statement.
 *
 * Returns: sql_stmt_t *, tail statement
 **/
sql_stmt_t *sql_stmt_tail( sql_stmt_t *stmt );

/**
 * Function: sql_stmt_finalize
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - sql_stmt_t *stmt: 	SQL-statement to finalize
 *
 * Description:
 * Finalizes an SQL-statement AKA closing it.
 *
 * Returns: void
 **/
void sql_stmt_finalize( sql_stmt_t *stmt );

#endif